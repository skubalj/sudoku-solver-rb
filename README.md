Sudoku Solver
=============

> THIS PROJECT IS STILL UNDER HEAVY DEVELOPMENT

This command line application serves as a simple solver for classic 9x9 
[Sudoku puzzles](https://en.wikipedia.org/wiki/Sudoku). Puzzles are read in as text files. The 
solution is displayed onscreen, as well as being written out to a file. 

An error message will be displayed if a puzzle is found to be unsolvable by the program. 

## License
This project is released under the terms of the MIT License. Please see the LICENSE 
file for more information.

Copyright (c) 2021 Joseph Skubal