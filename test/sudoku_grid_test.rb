# frozen_string_literal: true

require_relative '../lib/grid'

TEST_GRID = [
  [' ', ' ', ' ', ' ', ' ', ' ', ' ', '2', ' '],
  [' ', ' ', '6', '5', ' ', ' ', ' ', ' ', '7'],
  ['2', ' ', ' ', ' ', ' ', '4', ' ', '9', '8'],
  [' ', ' ', ' ', ' ', '7', '9', '5', ' ', ' '],
  [' ', ' ', '9', '6', ' ', '1', '2', ' ', ' '],
  [' ', ' ', '5', '8', '4', ' ', ' ', ' ', ' '],
  ['8', '7', ' ', '2', ' ', ' ', ' ', ' ', '6'],
  ['5', ' ', ' ', ' ', ' ', '7', '8', ' ', ' '],
  [' ', '4', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
].freeze

## Tests the `SudokuGrid` class
class SudokuGridTest < MiniTest::Test
  def test_init # rubocop:disable Metrics/MethodLength
    # Arrange
    # rubocop:disable Layout/TrailingWhitespace
    expected = "   |   | 2 
  6|5  |  7
2  |  4| 98
---+---+---
   | 79|5  
  9|6 1|2  
  5|84 |   
---+---+---
87 |2  |  6
5  |  7|8  
 4 |   |   "
    # rubocop:enable Layout/TrailingWhitespace

    # Act
    sut = SudokuGrid.new TEST_GRID

    # Assert
    assert_equal expected, sut.to_s
  end
end
