# frozen_string_literal: true

require_relative '../lib/file_loading'

class TestFileLoading < MiniTest::Test
  def test_grid_from_file # rubocop:disable Metrics/MethodLength
    # Arrange
    expected = [
      [' ', ' ', ' ', ' ', ' ', ' ', ' ', '2', ' '],
      [' ', ' ', '6', '5', ' ', ' ', ' ', ' ', '7'],
      ['2', ' ', ' ', ' ', ' ', '4', ' ', '9', '8'],
      [' ', ' ', ' ', ' ', '7', '9', '5', ' ', ' '],
      [' ', ' ', '9', '6', ' ', '1', '2', ' ', ' '],
      [' ', ' ', '5', '8', '4', ' ', ' ', ' ', ' '],
      ['8', '7', ' ', '2', ' ', ' ', ' ', ' ', '6'],
      ['5', ' ', ' ', ' ', ' ', '7', '8', ' ', ' '],
      [' ', '4', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
    ]

    # Act
    actual = FileLoading.grid_from_file './test/resources/valid_grid'

    # Assert
    assert_equal expected, actual
  end
end
