# frozen_string_literal: true

##
# Represents a single cell on the sudoku grid
class SudokuCell
  attr_accessor :shared_row, :shared_col, :shared_block
  attr_writer :value

  ##
  # Creates a new SudokuCell with the specified value
  #
  # @param value [Integer|Array] the initial value for this cell
  def initialize(value = %w[1 2 3 4 5 6 7 8 9])
    @shared_row = false
    @shared_col = false
    @shared_block = false

    @value = value
  end

  ##
  # @return [Boolean] `true` if this cell has been shared in all three ways
  def shared_all?
    @shared_row && @shared_col && @shared_block
  end

  ##
  # @return [Boolean] `true` if this cell is known
  def known?
    !@value.is_a?(Array)
  end

  ##
  # @return [Boolean] `true` if this cell could have the specified value
  def can_be?(value)
    if @value.is_a?(Array)
      # puts @value.join
      @value.include? value
    else
      @value == value
    end
  end

  ##
  # The value of this cell
  #
  # @return [Integer] the value for this cell or `nil` if this cell is not known
  def value
    known? ? @value : nil
  end

  ##
  # Removes the specified value from this cell's possibilities
  #
  # @return [Array] the possibilities that should be removed
  def remove_possibilities(possibilities)
    @value -= possibilities
    @value = @value[0] if @value.length == 1
  end

  ##
  # Converts this cell's value to a `String`
  #
  # @return [String] the `String` representation of this cell
  def to_s
    known? ? @value.to_s : ' '
  end
end
