# frozen_string_literal: true

##
# Represents a single solution area, either a row, column or block
#
# This class is not intended to be directly instantiated.
class SolutionArea
  def initialize(values)
    @cells = values
  end

  ##
  # Performs one iteration through this `SolutionArea`, reducing the number
  # of possibilities
  def iterate
    iterate_cells = @cells.partition(&:known?)
    found_values =
      iterate_cells[0]
      .filter { |cell| !share_if_not(cell) }
      .map(&:value)

    remove_found(found_values, iterate_cells[1])
    infer_needed(iterate_cells[0].map(&:value))
  end

  private

  def remove_found(to_remove, target_cells)
    target_cells.each { |cell| cell.remove_possibilities(to_remove) }
  end

  def infer_needed(known_values)
    (%w[1 2 3 4 5 6 7 8 9] - known_values)
      .each do |value|
      # puts value
      next unless iterate_cells[1].one? { |cell| cell.can_be? value }

      iterate_cells[1].find { |cell| cell.can_be? value }.value = value
    end
  end
end

##
# Represents a single row on the sudoku grid
class SudokuRow < SolutionArea
  def share_if_not(cell)
    previous = cell.shared_row
    cell.shared_row = true
    previous
  end
end

##
# Represents a single column on the sudoku grid
class SudokuColumn < SolutionArea
  def share_if_not(cell)
    previous = cell.shared_col
    cell.shared_col = true
    previous
  end
end

##
# Represents a single block on the sudoku grid
class SudokuBlock < SolutionArea
  def share_if_not(cell)
    previous = cell.shared_block
    cell.shared_block = true
    previous
  end
end
