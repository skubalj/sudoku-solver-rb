# frozen_string_literal: true

require_relative './solution_area'
require_relative './sudoku_cell'

##
# Represents an entire Sudoku Grid.
#
# This class exposes `SudokuGrid#iterate` which can be used repeatedly to
# solve a sudoku puzzle
class SudokuGrid
  ##
  # Creates a new SudokuGrid from the specified grid template
  #
  # @param grid [Array] the 2D array of characters which describes the grid
  def initialize(grid)
    @grid =
      grid.map do |row|
        row.map { |cell| cell == ' ' ? SudokuCell.new : SudokuCell.new(cell) }
      end

    @rows = @grid.map { |row| SudokuRow.new(row) }
    @cols = @grid.transpose.map { |col| SudokuColumn.new(col) }
    @blocks = make_blocks
  end

  ##
  # Returns `true` if this SudokuGrid has been solved
  #
  # A grid being 'solved' means that all cells are known
  #
  # @return [Boolean] `true` if this `SudokuGrid` has been solved
  def solved?
    @grid.all? { |row| row.all?(&:known?) }
  end

  ##
  # Returns `true` if this SudokuGrid has been determined to be unsolvable
  #
  # A grid being 'unsolvable' means that continued iterations will not change
  # the state of the grid. Every cell is either unknown, or has been shared
  #
  # @return [Boolean] `true` if this `SudokuGrid` is unsolvable
  def unsolvable?
    @grid.all? { |row| row.all? { |cell| !cell.known? || cell.shared_all? } }
  end

  ##
  # Performs one iteration through the grid, searching through all cells and
  # eliminating all possibilities that can be safely assumed
  #
  # Note that this method works through side effects.
  def iterate
    @rows.each(&:iterate)
    @cols.each(&:iterate)
    @blocks.each(&:iterate)
  end

  ##
  # Convert this `SudokuGrid` into a `String`
  #
  # @return [String] the representation of this `SudokuGrid`
  def to_s
    rows = @grid.map do |row|
      chars = row.map(&:to_s)
      (0..2).map { |offset| chars.slice(3 * offset, 3).join }.join '|'
    end

    (0..2).map do |offset|
      rows.slice(3 * offset, 3).join "\n"
    end.join "\n---+---+---\n"
  end

  private

  ##
  # Makes the array of `SudokuBlock` objects
  #
  # @return [Array] an array of `SudokuBlock` objects
  def make_blocks
    (0..2).map do |i|
      (0..2).map do |j|
        make_block(3 * i, 3 * j)
      end
    end.flatten
  end

  ##
  # Makes a single 3x3 block on the SudokuGrid
  #
  # @param x_offset [Integer] the x offset for the block to be created
  # @param x_offset [Integer] the y offset for the block to be created
  # @return [SudokuBlock] a sudoku block
  def make_block(x_offset, y_offset)
    block =
      (0..2).map do |i|
        (0..2).map do |j|
          @grid[x_offset + i][y_offset + j]
        end
      end.flatten
    SudokuBlock.new(block)
  end
end
