# frozen_string_literal: true

require_relative './grid'
require_relative './file_loading'

# Print Welcome Information
puts 'Sudoku Solver v0.1.0'

# Initial Prompt and Setup
print 'Enter source file name: '
source_file = gets.chomp
source_grid = FileLoading.grid_from_file source_file
grid = SudokuGrid.new source_grid

# Main Program Loop
until grid.solved? || grid.unsolvable?
  puts grid.to_s
  puts '----------------'
  grid.iterate
end

# Exit
puts grid.to_s
