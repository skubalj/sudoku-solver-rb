# frozen_string_literal: true

##
# A module which handles the loading of Grid objects to and from files
module FileLoading
  ## The regular expression describing a line of the grid
  GRID_LINE_REGEXP = /([\d ]{3})\|([\d ]{3})\|([\d ]{3})/.freeze

  ##
  # Converts the specified file into a 2D grid of numbers and spaces
  #
  # @param file_name [String] the name of the grid file
  # @return [Array] a 2D array of numbers and spaces
  def self.grid_from_file(file_name)
    file = File.open(file_name, 'r', chomp: true)
    grid = file
           .readlines
           .filter { |element| element =~ GRID_LINE_REGEXP }
           .map { |line| line_to_grid(line) }
    file.close
    grid
  end

  ##
  # Converts the specified line from the source file into a Grid
  #
  # @param line [String] the line from the source file
  # @return [Array] the grid row from this line
  private_class_method def self.line_to_grid(line)
    match_data = GRID_LINE_REGEXP.match line
    [match_data[1], match_data[2], match_data[3]]
      .flat_map { |set| set.split '' }
  end
end
